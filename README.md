## Init Config
1. Fork this repo to your account
2. Clone this repo to your local device
3. Run apps and should get error searching sdk from local.properties
4. create new file on root folder and name it local.properties, open with text editor add following code

```
sdk.dir=C\:\\Users\\DEV-Semanggi\\AppData\\Local\\Android\\Sdk
```

5. Adjust with your sdk location or copy from your other existing project
6. Run the apps and it should work fine
7. Create new feature branch from branch development
8. Check out to your feature branch
9. Happy coding... 

#### Code Convention
See the Code Convention that used [here](https://dev-codebase.web.app/convention/xml-convention/).

#### Workflow And Commit message
See the Gitflow that used [here](https://dev-codebase.web.app/convention/gitflow-use-case/).

